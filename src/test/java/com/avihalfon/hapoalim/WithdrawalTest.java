package com.avihalfon.hapoalim;

import com.avihalfon.hapoalim.model.WithdrawalInput;
import com.avihalfon.hapoalim.services.CancelWithdrawalServiceImpl;
import com.avihalfon.hapoalim.services.WithdrawalServiceImpl;
import com.avihalfon.hapoalim.utils.WithdrawalValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class WithdrawalTest {

    @Autowired
    CancelWithdrawalServiceImpl cancelWithdrawalService;
    @Autowired
    WithdrawalServiceImpl withdrawalServiceImp;

    private WithdrawalInput withdrawalInput;

    @BeforeEach
    public void setup() {
        withdrawalInput = WithdrawalInput.builder()
                .amount("200")
                .cardNumber("4580458045804580")
                .secretCodeNumber("1234")
                .build();
    }

    @Test
    void validation_test() throws Exception {
            WithdrawalValidator withdrawalValidator = new WithdrawalValidator();
            withdrawalValidator.validateInput(withdrawalInput);
    }

    @Test
    void db_test() throws Exception {
        cancelWithdrawalService.cancelWithdrawal(withdrawalInput);
        withdrawalServiceImp.withdrawal(withdrawalInput);
    }
}