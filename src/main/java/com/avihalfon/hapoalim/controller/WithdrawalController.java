package com.avihalfon.hapoalim.controller;

import com.avihalfon.hapoalim.exceptions.InvalidOperationException;
import com.avihalfon.hapoalim.exceptions.ReplyMessages;
import com.avihalfon.hapoalim.exceptions.UserInputException;
import com.avihalfon.hapoalim.model.WithdrawalInput;
import com.avihalfon.hapoalim.services.CancelWithdrawalServiceImpl;
import com.avihalfon.hapoalim.services.WithdrawalServiceImpl;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("atm")
@RequiredArgsConstructor
public class WithdrawalController {
    private static final Logger logger = LoggerFactory.getLogger(WithdrawalController.class);
    private final WithdrawalServiceImpl withdrawalService;
    private final CancelWithdrawalServiceImpl cancelWithdrawalService;

    @PostMapping("withdrawal")
    public ResponseEntity<?> withdraw(@Valid @RequestBody WithdrawalInput withdrawalInput) throws UserInputException, InvalidOperationException {
        logger.info("Calling withdraw request");
        withdrawalService.withdrawal(withdrawalInput);
        return new ResponseEntity<>(ReplyMessages.WITHDRAWAL_AMOUNT_SUCCESS.getMessage(),HttpStatus.OK);
    }

    @PostMapping("cancel")
    public ResponseEntity<?> cancel(@Valid @RequestBody WithdrawalInput withdrawalInput) throws UserInputException, InvalidOperationException {
        logger.info("Calling cancel withdraw request");
        cancelWithdrawalService.cancelWithdrawal(withdrawalInput);
        return new ResponseEntity<>(ReplyMessages.CANCEL_WITHDRAWAL.getMessage(),HttpStatus.OK);
    }
}
