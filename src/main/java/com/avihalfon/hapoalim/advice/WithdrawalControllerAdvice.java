package com.avihalfon.hapoalim.advice;

import com.avihalfon.hapoalim.exceptions.InternalException;
import com.avihalfon.hapoalim.exceptions.InvalidOperationException;
import com.avihalfon.hapoalim.exceptions.UserInputException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class WithdrawalControllerAdvice {
    private static final Logger logger = LoggerFactory.getLogger(WithdrawalControllerAdvice.class);
    @ExceptionHandler(value = {UserInputException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrDetails handleInputException(Exception e) {
        logger.info(e.getMessage());
        return new ErrDetails(e.getMessage());
    }

    @ExceptionHandler(value = {InvalidOperationException.class})
    @ResponseStatus(HttpStatus.TOO_MANY_REQUESTS)
    public ErrDetails handleInvalidOperationException(Exception e) {
        logger.info(e.getMessage());
        return new ErrDetails(e.getMessage());
    }

    @ExceptionHandler(value = {InternalException.class})
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    public ErrDetails handleInternalException(Exception e) {
        logger.info(e.getMessage());
        return new ErrDetails(e.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<List<String>> handleValidationExceptions(MethodArgumentNotValidException ex) {
        List<String> errors = ex.getBindingResult()
                .getAllErrors().stream()
                .map(ObjectError::getDefaultMessage)
                .collect(Collectors.toList());
        return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
    }
}
