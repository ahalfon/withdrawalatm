package com.avihalfon.hapoalim.clr;

import com.avihalfon.hapoalim.model.WithdrawalTransactions;
import com.avihalfon.hapoalim.repo.WithdrawalTransactionsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Arrays;

@Component
@RequiredArgsConstructor
public class InitData implements CommandLineRunner {
    private final WithdrawalTransactionsRepository withdrawalTransactionsRepository;

    @Override
    public void run(String... args) throws Exception {
        long rows = withdrawalTransactionsRepository.count();
        if(rows<1) {
            WithdrawalTransactions withdrawalTransactions1 = WithdrawalTransactions.builder()
                    .cardNumber("4580458045804580")
                    .secretCode("1234")
                    .amount(5000)
                    .maxLimitWithdrawalAmount(2000)
                    .amountWithdrew(0)
                    .lastModifiedDate(Date.valueOf(LocalDate.now()))
                    .build();

            WithdrawalTransactions withdrawalTransactions2 = WithdrawalTransactions.builder()
                    .cardNumber("4539532219313545")
                    .secretCode("9595")
                    .amount(5000)
                    .maxLimitWithdrawalAmount(2000)
                    .amountWithdrew(0)
                    .lastModifiedDate(Date.valueOf(LocalDate.now()))
                    .build();

            WithdrawalTransactions withdrawalTransactions3 = WithdrawalTransactions.builder()
                    .cardNumber("4716941216832263")
                    .secretCode("7754")
                    .amount(5000)
                    .maxLimitWithdrawalAmount(2000)
                    .amountWithdrew(0)
                    .lastModifiedDate(Date.valueOf(LocalDate.now()))
                    .build();

            WithdrawalTransactions withdrawalTransactions4 = WithdrawalTransactions.builder()
                    .cardNumber("4844876274813844")
                    .secretCode("8484")
                    .amount(5000)
                    .maxLimitWithdrawalAmount(2000)
                    .amountWithdrew(0)
                    .lastModifiedDate(Date.valueOf(LocalDate.now()))
                    .build();

            WithdrawalTransactions withdrawalTransactions5 = WithdrawalTransactions.builder()
                    .cardNumber("5459953216850037")
                    .secretCode("1254")
                    .amount(5000)
                    .maxLimitWithdrawalAmount(2000)
                    .amountWithdrew(0)
                    .lastModifiedDate(Date.valueOf(LocalDate.now()))
                    .build();

            withdrawalTransactionsRepository.saveAll(Arrays.asList(withdrawalTransactions1,
                    withdrawalTransactions2, withdrawalTransactions3, withdrawalTransactions4,
                    withdrawalTransactions5));
        }
    }
}