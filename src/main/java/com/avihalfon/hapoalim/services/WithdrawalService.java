package com.avihalfon.hapoalim.services;

import com.avihalfon.hapoalim.exceptions.InvalidOperationException;
import com.avihalfon.hapoalim.exceptions.UserInputException;
import com.avihalfon.hapoalim.model.WithdrawalInput;

public interface WithdrawalService {
    void withdrawal(WithdrawalInput withdrawalInput) throws UserInputException, InvalidOperationException;
}
