package com.avihalfon.hapoalim.services;

import com.avihalfon.hapoalim.exceptions.InvalidOperationException;
import com.avihalfon.hapoalim.exceptions.ReplyMessages;
import com.avihalfon.hapoalim.exceptions.UserInputException;
import com.avihalfon.hapoalim.model.WithdrawalInput;
import com.avihalfon.hapoalim.model.WithdrawalTransactions;
import com.avihalfon.hapoalim.repo.WithdrawalTransactionsRepository;
import com.avihalfon.hapoalim.utils.WithdrawalValidator;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CancelWithdrawalServiceImpl extends WithdrawalValidator implements CancelWithdrawalService {
    private static final Logger logger = LoggerFactory.getLogger(CancelWithdrawalServiceImpl.class);
    private final WithdrawalTransactionsRepository withdrawalTransactionsRepository;

    @Override
    public synchronized void cancelWithdrawal(WithdrawalInput withdrawalInput) throws UserInputException, InvalidOperationException {
        logger.info("Validating cancel request input");
        validateInput(withdrawalInput);

        WithdrawalTransactions withdrawalTransactions = withdrawalTransactionsRepository
                .findWithdrawalTransactionsByCardNumberAndSecretCode(withdrawalInput.getCardNumber(),withdrawalInput.getSecretCodeNumber());
        if(withdrawalTransactions == null) {
            throw new UserInputException(ReplyMessages.UNKNOWN_CARD_NUMBER.getMessage());
        }

        Integer amountWithdrew = withdrawalTransactions.getAmountWithdrew();
        Integer maxLimitWithdrawalAmount = withdrawalTransactions.getMaxLimitWithdrawalAmount();
        Integer accountAmount = withdrawalTransactions.getAmount();

        Integer amount = Integer.parseInt(withdrawalInput.getAmount());
        WithdrawalTransactions saveWithdrawalTransactions = WithdrawalTransactions.builder()
                .cardNumber(withdrawalInput.getCardNumber())
                .secretCode(withdrawalInput.getSecretCodeNumber())
                .amount(accountAmount+amount)
                .maxLimitWithdrawalAmount(maxLimitWithdrawalAmount)
                .amountWithdrew((amountWithdrew-amount<0)?0:amountWithdrew-amount)
                .build();

        withdrawalTransactionsRepository.save(saveWithdrawalTransactions);

        logger.info("Successfully saved cancelled request");
    }
}
