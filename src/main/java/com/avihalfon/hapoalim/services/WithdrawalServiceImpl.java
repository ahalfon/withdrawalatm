package com.avihalfon.hapoalim.services;

import com.avihalfon.hapoalim.component.WithdrawalRateLimiter;
import com.avihalfon.hapoalim.exceptions.ReplyMessages;
import com.avihalfon.hapoalim.exceptions.InvalidOperationException;
import com.avihalfon.hapoalim.exceptions.UserInputException;
import com.avihalfon.hapoalim.model.WithdrawalInput;
import com.avihalfon.hapoalim.model.WithdrawalTransactions;
import com.avihalfon.hapoalim.repo.WithdrawalTransactionsRepository;
import com.avihalfon.hapoalim.utils.WithdrawalValidator;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class WithdrawalServiceImpl extends WithdrawalValidator implements WithdrawalService {
    private static final Logger logger = LoggerFactory.getLogger(WithdrawalServiceImpl.class);
    private final WithdrawalRateLimiter withdrawalRateLimiter;
    private final WithdrawalTransactionsRepository withdrawalTransactionsRepository;
    @Override
    public synchronized void withdrawal(WithdrawalInput withdrawalInput) throws UserInputException,InvalidOperationException {
        logger.info("Validating withdrawal request input");
        validateInput(withdrawalInput);

        WithdrawalTransactions withdrawalTransactions = withdrawalTransactionsRepository
                .findWithdrawalTransactionsByCardNumberAndSecretCode(withdrawalInput.getCardNumber(),withdrawalInput.getSecretCodeNumber());
        if(withdrawalTransactions == null) {
            throw new UserInputException(ReplyMessages.UNKNOWN_CARD_NUMBER.getMessage());
        }

        Integer amountWithdrew = withdrawalTransactions.getAmountWithdrew();
        Integer maxLimitWithdrawalAmount = withdrawalTransactions.getMaxLimitWithdrawalAmount();
        Integer availableAmountToday = maxLimitWithdrawalAmount - amountWithdrew;
        Integer amount = Integer.parseInt(withdrawalInput.getAmount());
        Integer accountAmount = withdrawalTransactions.getAmount();
        if((accountAmount < amount) || (availableAmountToday<amount) ) {
            throw new UserInputException(ReplyMessages.NOT_ENOUGH_AMOUNT.getMessage());
        }

        if (!withdrawalRateLimiter.allowRequest(withdrawalInput.getCardNumber())) {
            throw new InvalidOperationException(ReplyMessages.TOO_MANY_REQUEST.getMessage());
        }

        WithdrawalTransactions saveWithdrawalTransactions = WithdrawalTransactions.builder()
                .cardNumber(withdrawalInput.getCardNumber())
                .secretCode(withdrawalInput.getSecretCodeNumber())
                .amount(accountAmount-amount)
                .maxLimitWithdrawalAmount(maxLimitWithdrawalAmount)
                .amountWithdrew(amountWithdrew+amount)
                .build();

        withdrawalTransactionsRepository.save(saveWithdrawalTransactions);
        logger.info("Successfully saved request");
    }
}
