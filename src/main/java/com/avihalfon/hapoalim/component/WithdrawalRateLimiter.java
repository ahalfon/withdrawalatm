package com.avihalfon.hapoalim.component;

import io.github.bucket4j.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Component
public class WithdrawalRateLimiter {
    private static final Logger logger = LoggerFactory.getLogger(WithdrawalRateLimiter.class);
    @Value("${ratelimit}")
    private Integer rateLimit;
    private final ConcurrentHashMap<String, Bucket> buckets = new ConcurrentHashMap<>();
    private final Lock resetLock = new ReentrantLock();

    public boolean allowRequest(String cardNumber) {
        Bucket bucket = buckets.computeIfAbsent(cardNumber, this::createBucket);
        ConsumptionProbe probe = bucket.tryConsumeAndReturnRemaining(1);
        return probe.isConsumed();
    }

    private Bucket createBucket(String cardNumber) {
        Bandwidth bandwidth = Bandwidth.simple(rateLimit, Duration.ofDays(1));
        return Bucket.builder().addLimit(bandwidth).build();
    }

    @Scheduled(cron = "0 0 0 * * *")
    public void resetRateLimit() {
        if (isAfterMidnight()) {
            resetBucket();
        }
    }
    private void resetBucket() {
        logger.info("Resetting he Bucket");
        if (resetLock.tryLock()) {
            try {
                buckets.entrySet()
                        .stream()
                        .forEach(b-> {
                                b.getValue().reset();
                            });
            } finally {
                resetLock.unlock();
            }
        }
    }
    private boolean isAfterMidnight() {
        LocalTime now = LocalTime.now();
        return now.isAfter(LocalTime.MIDNIGHT);
    }
}
