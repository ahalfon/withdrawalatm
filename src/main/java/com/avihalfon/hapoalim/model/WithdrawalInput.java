package com.avihalfon.hapoalim.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.CreditCardNumber;

import javax.validation.constraints.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WithdrawalInput {
    @NotNull(message = "Card Number cannot be null")
    @CreditCardNumber
    private String cardNumber;

    @NotNull(message = "Secret Code cannot be null")
    private String secretCodeNumber;

    @NotNull(message = "Withdrawal amount cannot be null")
    private String amount;
}
