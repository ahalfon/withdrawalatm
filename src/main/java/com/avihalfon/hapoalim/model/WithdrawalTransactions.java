package com.avihalfon.hapoalim.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Entity
@Table(name = "withdrawal_transactions")
public class WithdrawalTransactions {

    @Id
    private String cardNumber;

    private String secretCode;
    private Integer amount;
    private Integer amountWithdrew;
    private Integer maxLimitWithdrawalAmount;

    @UpdateTimestamp
    private Date lastModifiedDate;
}