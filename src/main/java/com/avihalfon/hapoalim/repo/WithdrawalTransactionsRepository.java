package com.avihalfon.hapoalim.repo;

import com.avihalfon.hapoalim.model.WithdrawalTransactions;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WithdrawalTransactionsRepository extends JpaRepository<WithdrawalTransactions, String> {
    WithdrawalTransactions findWithdrawalTransactionsByCardNumberAndSecretCode(String cardNumber, String secretCode);
}
