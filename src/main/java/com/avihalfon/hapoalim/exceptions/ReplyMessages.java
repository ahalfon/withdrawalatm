package com.avihalfon.hapoalim.exceptions;

import lombok.Getter;

@Getter
public enum ReplyMessages {
    AMOUNT_NOT_VALID("Invalid withdrawal amount"),
    NOT_ENOUGH_AMOUNT("Transaction cannot be completed due to insufficient funds"),
    //CARD_NUMBER_NOT_VALID("Invalid credit card number"),
    SECRET_CODE_NOT_VALID("Invalid secret code, must be a 4 digit number"),
    UNKNOWN_CARD_NUMBER("Unknown credit card"),
    TOO_MANY_REQUEST("Withdrawal not allowed more than 5 times in a day"),
    WITHDRAWAL_AMOUNT_SUCCESS("Withdrawal amount completed successfully"),
    CANCEL_WITHDRAWAL("Withdrawal cancellation completed successfully");;

    String message;
    ReplyMessages(String s) {
        this.message = s;
    }

    public String ReplyMessages() {
        return message;
    }
}
