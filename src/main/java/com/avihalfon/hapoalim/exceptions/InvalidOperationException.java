package com.avihalfon.hapoalim.exceptions;

public class InvalidOperationException extends Exception {
    public InvalidOperationException(String msg) {
        super(msg);
    }
}
