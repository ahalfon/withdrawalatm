package com.avihalfon.hapoalim.exceptions;

public class InternalException extends Exception {
    public InternalException(String msg) {
        super(msg);
    }
}
