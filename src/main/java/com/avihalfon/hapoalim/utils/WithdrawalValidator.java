package com.avihalfon.hapoalim.utils;

import com.avihalfon.hapoalim.exceptions.ReplyMessages;
import com.avihalfon.hapoalim.exceptions.UserInputException;
import com.avihalfon.hapoalim.model.WithdrawalInput;

public class WithdrawalValidator {
    public void validateInput(WithdrawalInput withdrawalInput) throws UserInputException {
        try {
            Integer amount = Integer.parseInt(withdrawalInput.getAmount());
            if(amount<1) {
                throw new UserInputException(ReplyMessages.AMOUNT_NOT_VALID.getMessage());
            }
        } catch (NumberFormatException e) {
            throw new UserInputException(ReplyMessages.AMOUNT_NOT_VALID.getMessage());
        }

        String secretCode = withdrawalInput.getSecretCodeNumber();
        if (!secretCode.matches("\\d{4}")) {
            throw new UserInputException(ReplyMessages.SECRET_CODE_NOT_VALID.getMessage());
        }
    }
}
